import React, { Component } from "react";
import Aux from "../../Aux";
import classes from "../Layout/Layout.css";
import MyCarousel from "../../components/MyCarousel/MyCarousel"
import Presenter from "../../components/Presenter/Presenter"

export default class Layout extends Component {

    state = {
        currentContent: {
            title: "Please select a box",
            value: "This is gonna be value",
            currentBox: null
        },
        headerColor: "aqua"
        
    }

  handleCarouselItemClick = (box, val) => {
      console.log(box, val)
      this.setState({currentContent: {
          title: box,
          value: val
      }})
  }

  changeColor = (e) => {
      if(e.key == "Enter") {
          console.log(e.target.value)
          this.setState({
              headerColor: e.target.value
          })
        e.target.value = "";
      }
  }



  render() {
    return (
      <div className={classes.Layout}>
        <header style={{backgroundColor: this.state.headerColor}}>
          <h1>This is header</h1>
          <input type="text" onKeyPress={this.changeColor}/>
        </header>
        <section>
          <aside>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. In
            venenatis sem sed consectetur suscipit. Nunc blandit tellus purus.
            Vestibulum pretium risus eget ultricies consequat. Quisque a velit
            eu elit ullamcorper sagittis vel ut eros. Sed malesuada facilisis
            nisl elementum feugiat. 
          </aside>
          <main>
            <Presenter content={this.state.currentContent}/>
            <MyCarousel title="Hello" clicked={this.handleCarouselItemClick} times={3}/>
            <MyCarousel title="Wuzzup" clicked={this.handleCarouselItemClick} times={5} /> 
          </main>
        </section>
        <footer>This is footer!</footer>
      </div>
    );
  }
}
