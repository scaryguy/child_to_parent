import React, { Component } from "react";

export default class Box extends Component {


  state = {
    bgColor: "white"
  };

  interval = null;

  onEnterHandler = e => {
    
    this.startAnimation();
    this.setState({ bgColor: "white" });
    this.props.updateColor("white");
  };

  onOutHandler = e => {
    clearInterval(this.interval);

    this.setState({
      bgColor: "white"
    });
    this.props.updateColor("white");
  };

  startAnimation = () => {
    const COLORS = ["pink", "red", "purple", "green"];

    this.interval = setInterval(() => {
      const newColor =
        COLORS[Math.floor(Math.random() * Math.floor(COLORS.length))];
        
      this.props.updateColor(newColor);
      this.setState({ bgColor: newColor });
    }, 1000);

    // this.setState(prevState => {
    //   return {
    //     bgColor: "white"
    //   };
    // });
  };

  render() {
    return (
      <div
        onMouseEnter={this.onEnterHandler}
        style={{
          backgroundColor: this.state.bgColor
        }}
        onMouseLeave={this.onOutHandler}
      >
        {this.props.value}
      </div>
    );
  }
}
