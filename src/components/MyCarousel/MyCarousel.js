import React, { Component } from "react";
import Aux from "../../Aux";
import classes from "../MyCarousel/MyCarousel.css";
import Box from "../Box/Box";

export default class MyCarousel extends Component {
  state = {
    currentColor: "white"
  };


  updateColor = (color)=> {
    this.setState({ currentColor: color });
    this.props.clicked("Current color for last selected box is:", this.state.currentColor)
  }
  

  render() {
    const title = !this.props.present ? <h1>{this.props.title}</h1> : null;
    const arr = new Array(+this.props.times).fill(undefined).map((x, i) => {
      return i + 1;
    });

    return (
      <Aux>
        {title}
        <div className={classes.MyCarousel} >

          {arr.map(x => (
            <Box
              key={x}
              value={x}
              updateColor={this.updateColor}
            />
          ))}
          
        </div>
      </Aux>
    );
  }
}
