import React from 'react'
import classes from "./Presenter.css"

export default (props) => {
  return (
    <div className={classes.Presenter}>
      <h1>{props.content.title}</h1>
      <p>{props.content.value}</p>
    </div>
  )
}
