import React, { Component } from "react";
import Layout from "./containers/Layout/Layout";
import classes from "./App.css"
class App extends Component {
  render() {
    return (
      <Layout className={classes}/>
    );
  }
}

export default App;
